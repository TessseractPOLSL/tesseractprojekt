<?php
App::uses('AppModel', 'Model');

class File extends AppModel {
	public $useTable = 'files';
	
    public $validate = array(
        'filename' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'Filename is required'
            )
        ),
        'user_id' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'User id is required'
            )
        )
    );
}