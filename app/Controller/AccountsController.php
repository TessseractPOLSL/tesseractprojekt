<?php
App::uses('AppController', 'Controller');

class AccountsController extends AppController {
	
	var $layout = 'tesseract';
	public $uses = array('File');
	
    public function beforeFilter() {
		parent::beforeFilter();
		
		// Allow users to register and logout.
		//$this->Auth->allow('add', 'logout');
	}

    public function index() {
		$userId = $this->Session->read('Auth.User.id');
		
		$fileList = $this->File->find('all', array(
			'conditions' => array(
				'File.user_id' => $userId
			),
			'fields' => array(
				'id',
				'filename',
				'user_id',
				'LEFT(File.ocr, 5) as ocr'
			)
		));
		//echo $this->webroot;
        //print_r($fileList);
		$this->set('files', $fileList);
    }
	
	public function upload(){
		
		if (!$this->request->is('post')) // form submitted?
			return;
		
		$file = $this->data['Account']['file'];
		
		if (is_null($file) || empty($file)){
			$this->Session->setFlash('Brak pliku');
			$this->redirect(array('action' => 'index'));
			return;
		}
			
		if ($file['type'] != 'image/jpeg'){
			$this->Session->setFlash('Błędne rozszerzenie pliku. Wymagane to JPEG');
			$this->redirect(array('action' => 'index'));
			return;
		}
		
		$date = new DateTime();
		$filename = $date->getTimestamp().'.jpg';
		$filepath = WWW_ROOT.'/files/';
		
		if (!(move_uploaded_file($file['tmp_name'], $filepath.$filename))){
			$this->Session->setFlash('There was a problem uploading file. Please try again.');
			$this->redirect(array('action' => 'index'));
			return;
		}
	
		/* Store data in DB */
		$this->File->set(array(
			'filename' => $filename,
			'user_id' => $this->Session->read('Auth.User.id')
		));
		$this->File->save();
		
		$this->Session->setFlash('File uploaded successfuly.');
		$this->redirect(array('action' => 'index'));
	}
	
	public function remove($id){
		$userId = $this->Session->read('Auth.User.id');
		
		$file = $this->File->find('first', array(
			'conditions' => array(
				'File.id' => $id
			),
			'fields' => array(
				'id',
				'filename',
				'user_id'
			)
		));
		
		if(empty($file)){
			$this->Session->setFlash('Brak pliku.');
			$this->redirect(array('action' => 'index'));
		}
		
		if($file['File']['user_id'] != $userId){
			$this->Session->setFlash('Nie moge usunąć pliku.');
			$this->redirect(array('action' => 'index'));
		}
		
		// Remove DB entry
		$this->File->delete($id);
		
		// Remove file
		$fullFilePath = WWW_ROOT.'/files/'.$file['File']['filename'];
		unlink($fullFilePath);
		
		$this->Session->setFlash('Plik usunięty.');
		$this->redirect(array('action' => 'index'));
	}
	
	public function ocr($id){
		$userId = $this->Session->read('Auth.User.id');
		
		$file = $this->File->find('first', array(
			'conditions' => array(
				'File.id' => $id
			),
			'fields' => array(
				'id',
				'filename',
				'user_id'
			)
		));
		
		if(empty($file)){
			$this->Session->setFlash('Brak pliku.');
			$this->redirect(array('action' => 'index'));
		}
		
		if($file['File']['user_id'] != $userId){
			$this->Session->setFlash('Nie moge dokonac OCR.');
			$this->redirect(array('action' => 'index'));
		}
		
		$ocrString = file_get_contents('http://localhost:8888/ocr/'.$file['File']['filename'].'/tajnehaslo');
		
		$this->File->save(array(
			'id' => $id,
			'ocr' => $ocrString
		));
		
		$this->Session->setFlash('Dokonano OCR.');
		$this->redirect(array('action' => 'index'));
	}
	
	public function show($id){
		$userId = $this->Session->read('Auth.User.id');
		
		$file = $this->File->find('first', array(
			'conditions' => array(
				'File.id' => $id
			),
			'fields' => array(
				'id',
				'filename',
				'user_id',
				'ocr'
			)
		));
		
		if(empty($file)){
			$this->Session->setFlash('Brak pliku.');
			$this->redirect(array('action' => 'index'));
		}
		
		$file = $file['File'];
		
		if($file['user_id'] != $userId){
			$this->Session->setFlash('Nie moge wyświetlić pliku.');
			$this->redirect(array('action' => 'index'));
		}
		
		$this->set('file', $file);
	}
}