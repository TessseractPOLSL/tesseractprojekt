<?php
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		
	</title>
	<?php

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	
	<!-- Bootstrap core CSS -->
	<?php echo $this->Html->css(array('bootstrap.css', 'templatemo_style.css'));?>

	<!-- Custom styles for this template -->
	<link href="<?php echo $this->webroot ?>js/colorbox/colorbox.css"  rel='stylesheet' type='text/css'>
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>
<div class="templatemo-top-menu">
            <div class="container">
                <!-- Static navbar -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <a href="#" class="navbar-brand"><img src="<?php echo $this->webroot ?>images/tesseract-top.jpg" alt="" title="" /></a>
                        </div>
                        <div class="navbar-collapse collapse" id="templatemo-nav-bar">
                            <ul class="nav navbar-nav navbar-right" style="margin-top: 40px;">
                                <li class="active"><a href="<?php echo $this->Html->url('/', true); ?>">HOME</a></li>
								<?php if ($isUserLogged): ?>
									<li><a href="<?php echo $this->Html->url('/accounts', true); ?>">KONTO</a></li>
									<li><a href="<?php echo $this->Html->url('/users/logout', true); ?>">WYLOGUJ</a></li>
								<?php else: ?>
									<li><a href="<?php echo $this->Html->url('/users/add', true); ?>">REJESTRUJ</a></li>
									<li><a href="<?php echo $this->Html->url('/users/login', true); ?>">ZALOGUJ</a></li>
								<?php endif; ?>
						   </ul>
                        </div><!--/.nav-collapse -->
                    </div><!--/.container-fluid -->
                </div><!--/.navbar -->
            </div> <!-- /container -->
        </div>
        
		<div class="container">
			<?php 
				$flashMessage = $this->Session->flash(); 
				if(!empty($flashMessage)): ?>
				<div class="alert alert-info">
					<h4 style="margin: 0;"><?php echo $flashMessage; ?></h4>
				</div>
			<?php endif; ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		
        <div class="templatemo-footer" >
            <div class="container">
                <div class="row">
                    <div class="text-center">
                        <div class="">
							<div class="height30"></div>
                            <a class="btn btn-lg btn-orange" href="#" role="button" id="btn-back-to-top">Back To Top</a>
                            <div class="height30"></div>
                        </div>
                        <div class="footer_bottom_content">
                        	Copyright © 2015 <a href="#"><strong>BDIS-2</strong></a> 
                        	- Design: Rychu
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo $this->webroot ?>js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->webroot ?>js/bootstrap.min.js"  type="text/javascript"></script>
        <script src="<?php echo $this->webroot ?>js/stickUp.min.js"  type="text/javascript"></script>
        <script src="<?php echo $this->webroot ?>js/colorbox/jquery.colorbox-min.js"  type="text/javascript"></script>
        <!--<script src="<?php echo $this->webroot ?>js/templatemo_script.js"  type="text/javascript"></script>-->

	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
