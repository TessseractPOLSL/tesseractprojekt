Prześlij plik:
<?php 
	$form = $this->form;
	echo $form->create('Account', array('action' => 'upload', 'type' => 'file'));
?>
<input type="file" name="data[Account][file]" id="AccountFile" accept="image/*;capture=camera" class="btn-primary" /><br>
<input type="submit" value="Upload" class="btn btn-success">
<?php echo $form->end(); ?>
<br>

<table class="table table-striped table-bordered table-hover" style="white-space: nowrap">
	<thead>
		<tr>
			<td><b>Plik</b></td>
			<td><b>Tekst OCR</b></td>
			<td><b>Akcje</b></td>
		</tr>
	</thead>
	
	<tbody>
		<?php foreach($files as $row): ?>
		<tr>
			<td><?php echo $row['File']['filename'] ?></td>
			<td><?php echo $row[0]['ocr'] ?>(...)</td>
			<td style="width: 200px;">
				<a href="/tesseract/accounts/show/<?php echo $row['File']['id'];?>" class="btn btn-xs btn-primary">Wyświetl</a>
				
				<?php if(empty($row[0]['ocr'])): ?>
					<a href="/tesseract/accounts/ocr/<?php echo $row['File']['id'];?>" class="btn btn-xs btn-warning">Uruchom OCR</a>
				<?php endif;?>
				
				<a href="/tesseract/accounts/remove/<?php echo $row['File']['id'];?>" class="btn btn-xs btn btn-danger">Usuń</a>
			</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
