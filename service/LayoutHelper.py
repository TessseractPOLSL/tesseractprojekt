#-*- coding: utf-8 -*-

class LayoutHelper:
	def __init__(self, file):
		if not file:
			return "File path not specified!";
		
		self.file = file
		
	def render(self, title, content):
		with open (self.file, "r") as layoutFile:
			data = layoutFile.read().replace('{title}', title.encode('utf-8')).replace('{content}', content.encode('utf-8'))
		
		return data