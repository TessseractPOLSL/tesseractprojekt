#-*- coding: utf-8 -*-

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.autoreload

import pprint
#from tesseract import image_to_string
import pytesseract
from PIL import Image
import StringIO
import os.path
import uuid
import subprocess

from LayoutHelper import LayoutHelper

class MainHandler(tornado.web.RequestHandler):
	def get(self):
		content = ('Send us a file!<br/><form enctype="multipart/form-data" action="/" method="post">'
				   '<input type="file" name="the_file"><br>'
				   '<input type="submit" value="Submit">'
				   '</form>')
				   
		self.write(layoutHelper.render(u'Strona główna', content))

	def post(self):
		self.set_header("Content-Type", "text/html")
		self.write("<html><body>")
		self.write("You sent a file with name " + self.request.files.items()[0][1][0]['filename'] +"<br/>" )
	
		# create a unique ID file
		tempname = str(uuid.uuid4()) + ".jpg"
		myimg = Image.open(StringIO.StringIO(self.request.files.items()[0][1][0]['body']))
		myfilename = os.path.join(os.path.dirname(__file__),"static",tempname);
                
		# save image to file as JPEG
		myimg.save(myfilename)

		self.write("<img src=\"static/" + tempname + "\" /><br/>") 

		# do OCR, print result
		self.write(pytesseract.image_to_string(myimg))
		self.write("</body></html>")

class VersionHandler(tornado.web.RequestHandler):
	def get(self):
		cmd = ['tesseract', '-v']
		tesseractProcess = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		out, err = tesseractProcess.communicate()
		version = err
		
		print version.split("\r\n")
		info = version.split("\r\n")
		name = info[0]
		build = info[1]
		libs = info[2].split(" : ")
		print libs
		#self.write(version.replace("\r\n", "<br>"))
		
		content = ('<h3> Build: <br>'+build+'</h3><br>'
				   '<p>Biblioteki:  <br>'+info[2]+'</p>')
		
		self.write(layoutHelper.render(u'Wersja - '+name, content))

class ExitHandler(tornado.web.RequestHandler):
	def get(self):
		print 'Server stopped! See you'
		
		tornado.ioloop.IOLoop.instance().stop()
		
class LanguageHandler(tornado.web.RequestHandler):
	def get(self):
		cmd = ['tesseract', '--list-langs']
		
		tesseractProcess = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		out, err = tesseractProcess.communicate()
		langs = err
		print langs
		
		self.write(layoutHelper.render(u'Języki rozpoznawania', langs.replace("\r\n", "<br>")))
		

class HelpHandler(tornado.web.RequestHandler):
	def get(self):
		cmd = ['tesseract']
		tesseractProcess = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		out, err = tesseractProcess.communicate()
		helpInfo = err
		helpInfo = helpInfo.replace("\r\n", "<br>")
		helpInfo = helpInfo.split("<br><br>")
		
		content = ('<p>'+
				   helpInfo[0]
				   +'<br><br>'+
				   helpInfo[2]+
				   '</p>')
		
		self.write(layoutHelper.render(u'Pomoc - komendy', content))

class PagesegmodHandler(tornado.web.RequestHandler):
	def get(self):
		cmd = ['tesseract']
		tesseractProcess = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
		out, err = tesseractProcess.communicate()
		helpInfo = err
		helpInfo = helpInfo.replace("\r\n", "<br>")
		helpInfo = helpInfo.split("<br><br>")
		
		content = ('<p>'+
				   helpInfo[1]+
				   '</p>')
		
		self.write(layoutHelper.render(u'Ustawienia podziału strony', content))
		
class AboutHandler(tornado.web.RequestHandler):
	def get(self):
		content = ('<p> <h3>Tesseract OCR Service </h3><br>'
				   '<strong>Twórcy:</strong> <br><ul>'
				   '<li>Dominika Fusek</li>'
				   '<li>Paweł Rychlewski</li>'
				   '<li>Grzegorz Sztandera</li></ul>'
				   '<u>BDIS - S 2014/2015</u>'
				   '</p>')
		content = unicode(content, 'utf-8')
		
		self.write(layoutHelper.render(u'Informacje o programie', content))

class OCRHandler(tornado.web.RequestHandler):
	def get(self, fileName, secret):
		self.set_header("Content-Type", "text/plain; charset=utf-8")
		
		if secret != "tajnehaslo":
			self.write('Brak dostepu: bledne haslo')
			#self.finish()
			return
		
		filePath = os.path.join(os.path.dirname(__file__), "../app/webroot/files", fileName);
		
		if not os.path.isfile(filePath):
			self.write('Plik nie istnieje')
			return
		
		myimg = Image.open(filePath)
		resultString = pytesseract.image_to_string(myimg)
		
		self.write(resultString)
		
layoutHelper = LayoutHelper("szablon/szablon.html")

settings = {
	"static_path": os.path.join(os.path.dirname(__file__), "../app/webroot/files")
}

application = tornado.web.Application([
	(r"/",			MainHandler),
	(r"/index",		MainHandler),
	(r"/about",		AboutHandler),
	(r"/listlangs", LanguageHandler),
	(r"/version", 	VersionHandler),
	(r"/pagesegmod",PagesegmodHandler),
	(r"/help",		HelpHandler),
	(r"/exit",		ExitHandler),
	
	(r"/ocr/([^/]+)/([^/]+)", OCRHandler),
	
	(r'/css/(.*)',		tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "szablon/css")}),
	(r'/js/(.*)',		tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "szablon/js")}),
	(r'/images/(.*)',	tornado.web.StaticFileHandler, {'path': os.path.join(os.path.dirname(__file__), "szablon/images")})
], **settings)

if __name__ == "__main__":
	print "Starting tornado server"
	http_server = tornado.httpserver.HTTPServer(application)
	http_server.listen(8888)
	print "Bind server to port 8888"
	#TODO remove in prod
	tornado.autoreload.start()
	
	print "Server running"
	tornado.ioloop.IOLoop.instance().start()
	